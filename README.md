# README #

Скопировать репозиторий:

```
git clone https://xxlabaza@bitbucket.org/xxlabaza/spring-cloud-netflix-oss-example.git
cd spring-cloud-netflix-oss-example
```

### Config Service ###

[Config Service](https://bitbucket.org/xxlabaza/spring-cloud-netflix-oss-example/src/5c761dc304a35f915fa7ee395541827361839bfd/service-config/?at=master) представляет собой менеджер настроек. Сами конфиги находятся в репозитории, в папку [configuration](https://bitbucket.org/xxlabaza/spring-cloud-netflix-oss-example/src/5c761dc304a35f915fa7ee395541827361839bfd/configuration/?at=master)

Из особенностей стоит отметить, что к данному сервису осуществляется доступ *только* через логин/пароль указанных в его [bootstrap.yml](https://bitbucket.org/xxlabaza/spring-cloud-netflix-oss-example/src/5c761dc304a35f915fa7ee395541827361839bfd/service-config/src/main/resources/bootstrap.yml?at=master)

**Запуск сервиса:**

```
cd service-config/
mvn clean && mvn package
java -jar target/service-config-1.0.0.jar
```

После запуска **Config Service** начнет писать ошибки в stdout:

```
2015-07-07 14:15:48.516 ERROR 1202 --- [pool-5-thread-1] com.netflix.discovery.DiscoveryClient    : DiscoveryClient_CONFIG/192.168.236.8:config:163fdacee5170092565d02daf7329736 - was unable to refresh its cache! status = java.net.ConnectException: Connection refused

com.sun.jersey.api.client.ClientHandlerException: java.net.ConnectException: Connection refused
	at com.sun.jersey.client.apache4.ApacheHttpClient4Handler.handle(ApacheHttpClient4Handler.java:184)
	at com.sun.jersey.api.client.filter.GZIPContentEncodingFilter.handle(GZIPContentEncodingFilter.java:120)
	at com.netflix.discovery.EurekaIdentityHeaderFilter.handle(EurekaIdentityHeaderFilter.java:28)
	at com.sun.jersey.api.client.Client.handle(Client.java:648)
	at com.sun.jersey.api.client.WebResource.handle(WebResource.java:680)
	at com.sun.jersey.api.client.WebResource.access$200(WebResource.java:74)
	at com.sun.jersey.api.client.WebResource$Builder.get(WebResource.java:507)
	at com.netflix.discovery.DiscoveryClient.getUrl(DiscoveryClient.java:1567)
	at com.netflix.discovery.DiscoveryClient.makeRemoteCall(DiscoveryClient.java:1113)
	at com.netflix.discovery.DiscoveryClient.makeRemoteCall(DiscoveryClient.java:1060)
	at com.netflix.discovery.DiscoveryClient.getAndStoreFullRegistry(DiscoveryClient.java:835)
	at com.netflix.discovery.DiscoveryClient.fetchRegistry(DiscoveryClient.java:746)
	at com.netflix.discovery.DiscoveryClient.access$1400(DiscoveryClient.java:105)
	at com.netflix.discovery.DiscoveryClient$CacheRefreshThread.run(DiscoveryClient.java:1723)
	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
	at java.util.concurrent.FutureTask.run(FutureTask.java:266)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
	at java.lang.Thread.run(Thread.java:745)
Caused by: java.net.ConnectException: Connection refused
	at java.net.PlainSocketImpl.socketConnect(Native Method)
	at java.net.AbstractPlainSocketImpl.doConnect(AbstractPlainSocketImpl.java:345)
	at java.net.AbstractPlainSocketImpl.connectToAddress(AbstractPlainSocketImpl.java:206)
	at java.net.AbstractPlainSocketImpl.connect(AbstractPlainSocketImpl.java:188)
	at java.net.SocksSocketImpl.connect(SocksSocketImpl.java:392)
	at java.net.Socket.connect(Socket.java:589)
	at org.apache.http.conn.scheme.PlainSocketFactory.connectSocket(PlainSocketFactory.java:117)
	at org.apache.http.impl.conn.DefaultClientConnectionOperator.openConnection(DefaultClientConnectionOperator.java:177)
	at org.apache.http.impl.conn.AbstractPoolEntry.open(AbstractPoolEntry.java:144)
	at org.apache.http.impl.conn.AbstractPooledConnAdapter.open(AbstractPooledConnAdapter.java:131)
	at org.apache.http.impl.client.DefaultRequestDirector.tryConnect(DefaultRequestDirector.java:611)
	at org.apache.http.impl.client.DefaultRequestDirector.execute(DefaultRequestDirector.java:446)
	at org.apache.http.impl.client.AbstractHttpClient.doExecute(AbstractHttpClient.java:863)
	at org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:115)
	at org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:57)
	at com.sun.jersey.client.apache4.ApacheHttpClient4Handler.handle(ApacheHttpClient4Handler.java:170)
	... 18 common frames omitted
```

Эти ошибки означают, что он не смог найти **Eureka Service**, но постоянно пытается, молодец. Он будет стараться достучаться до **Eureka Service** по указанному нами **hertbeat**'у в [общих настройках всех сервисов](https://bitbucket.org/xxlabaza/spring-cloud-netflix-oss-example/src/5c761dc304a35f915fa7ee395541827361839bfd/configuration/application.yml?at=master).

Можно отключить возможность **Config Service**'а регистрации в **Eureka**, в принципе это, в нашем примере, и не нужно, так как все остальные сервисы ходят к конфигу напрямую - по его хосту и порту (см. любой **bootstrap.yml**)

### Eureka Service ###

[Eureka Service](https://bitbucket.org/xxlabaza/spring-cloud-netflix-oss-example/src/5c761dc304a35f915fa7ee395541827361839bfd/service-eureka/?at=master) представляет из себя **Discovery Service**. В нём осуществляется регистрация всех сервисов, благодаря чему любой другой сервис может узнать где находится нужный ему.

**Запуск сервиса:**

```
cd service-eureka 
mvn clean && mvn package
java -jar target/service-eureka-1.0.0.jar
```

Сразу после старта, **Config Service** "успокоится" и зарегестрируется в **Eureka Service**:

```
2015-07-07 14:28:38.721  INFO 1202 --- [pool-4-thread-1] com.netflix.discovery.DiscoveryClient    : DiscoveryClient_CONFIG/192.168.236.8:config:163fdacee5170092565d02daf7329736 - Re-registering apps/CONFIG
2015-07-07 14:28:38.721  INFO 1202 --- [pool-4-thread-1] com.netflix.discovery.DiscoveryClient    : DiscoveryClient_CONFIG/192.168.236.8:config:163fdacee5170092565d02daf7329736: registering service...
2015-07-07 14:28:38.775  INFO 1202 --- [pool-4-thread-1] com.netflix.discovery.DiscoveryClient    : DiscoveryClient_CONFIG/192.168.236.8:config:163fdacee5170092565d02daf7329736 - registration status: 204
```

Проверить, что **Config Service** зарегестрировался в **Eureka** мы можем простой командой:

```
curl -s -H "Accept: application/json" http://localhost:8761/eureka/apps | jq '.[][][] | {name: .name, url: .instance.homePageUrl, status: .instance.status}'

{
  "name": "CONFIG",
  "url": "http://192.168.236.8:9876/",
  "status": "UP"
}
{
  "name": "EUREKA",
  "url": "http://192.168.236.8:8761/",
  "status": "UP"
}
```

В ответе мы увидим все зарегистрированные сервисы в **Eureka**.

Но можно проще - ввести в адресную строку браузера URL: **http://localhost:8761** и увидеть красивую страничку)

### Client Service ###

[Client Service](https://bitbucket.org/xxlabaza/spring-cloud-netflix-oss-example/src/5c761dc304a35f915fa7ee395541827361839bfd/service-client/?at=master) выполняет роль тестового приложения, использующего [Feign](https://github.com/Netflix/feign), [Ribbon](https://github.com/Netflix/ribbon) и [Hystrix](https://github.com/Netflix/Hystrix).

Для испытаний нам потребуется запустить сразу два инстанса сервиса.

Билдим проект:

```
cd service-client/
mvn clean && mvn package
```

В одном терминале запускаем первый инстанс:

```
java -jar target/service-client-1.0.0.jar --spring.profiles.active=first
```

Во втором второй:
```
java -jar target/service-client-1.0.0.jar --spring.profiles.active=second
```

> Настройки портов прописаны в [client.yml](https://bitbucket.org/xxlabaza/spring-cloud-netflix-oss-example/src/5c761dc304a35f915fa7ee395541827361839bfd/configuration/client.yml?at=master).
>
> Никто не запрещает запускать без опции **--spring.profiles.active=**, но тогда сервис будет стартовать на порту **8080**.

После старта сервисов можно проверить их работу:

> Об успешном старте сервиса говорит сообщение в консоли:
>
> ```
> DiscoveryClient_CLIENT/192.168.236.8:client:72b832666042203bbb033c40ca4ecbdf - Re-registering apps/CLIENT
> DiscoveryClient_CLIENT/192.168.236.8:client:72b832666042203bbb033c40ca4ecbdf: registering service...
> DiscoveryClient_CLIENT/192.168.236.8:client:72b832666042203bbb033c40ca4ecbdf - registration status: 204
> DiscoveryClient_CLIENT/192.168.236.8:client:72b832666042203bbb033c40ca4ecbdf - retransmit instance info with status UP
> DiscoveryClient_CLIENT/192.168.236.8:client:72b832666042203bbb033c40ca4ecbdf: registering service...
> DiscoveryClient_CLIENT/192.168.236.8:client:72b832666042203bbb033c40ca4ecbdf - registration status: 204
> DiscoveryClient_CLIENT/192.168.236.8:client:72b832666042203bbb033c40ca4ecbdf - Re-registering apps/CLIENT
> DiscoveryClient_CLIENT/192.168.236.8:client:72b832666042203bbb033c40ca4ecbdf: registering service...
> DiscoveryClient_CLIENT/192.168.236.8:client:72b832666042203bbb033c40ca4ecbdf - registration status: 204
> ```
>
> Означающее, что сервис зарегестрировался в **Eureka** и три раза обменялся с ней **heartbeat**'ом

```
curl localhost:8181/who
client:8384fc678d801772b79f00cfa40d1c07   # в ответ на /who мы получаем eurekaid

curl localhost:8282/who
client:3fa2ea78f9a21987e72b3259ef437627   # соответственно у второго сервиса этот id будет другим

curl localhost:8181/feign/who
client:3fa2ea78f9a21987e72b3259ef437627   # запрос /feign/who вернет нам eurekaid инстанса client, выбранного Ribbon'ом по round-robin

curl localhost:8181/feign/who
client:8384fc678d801772b79f00cfa40d1c07

curl localhost:8181/feign/who
client:3fa2ea78f9a21987e72b3259ef437627

curl localhost:8181/discovery/who
client:8384fc678d801772b79f00cfa40d1c07   # запрос /discovery/who вернет eurekaid первого зарегестрированного инстанса client, найдя его не через Feign+Ribbon, а только средствами Eureka

curl localhost:8181/hystrix/ok
GOOD EXECUTION HYSTRIX METHOD             # запрос означает успешное выполнение hystrix-метода

curl localhost:8181/hystrix/nok
BAD EXECUTION HYSTRIX METHOD with request: nok  # неуспешное выполнение hystrix-метода, сработа fallback-метод-заглушка
```