package ru.xxlabaza.service.client.example;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 *
 * @author Artem Labazin
 *
 * @since Jul 7, 2015 | 1:00:36 PM
 *
 * @version 1.0.0
 */
@FeignClient("client")
interface ClientApi {

    @RequestMapping(
            value = "/who",
            method = GET
    )
    String askWho ();
}
