package ru.xxlabaza.service.client;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

/**
 *
 * @author Artem Labazin
 *
 * @since Jul 7, 2015 | 12:58:20 PM
 *
 * @version 1.0.0
 */
@EnableHystrix
@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class Main {

    public static void main (String[] args) {
        new SpringApplicationBuilder(Main.class)
                .web(true)
                .run(args);
    }
}
