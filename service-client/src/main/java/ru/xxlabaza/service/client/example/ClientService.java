package ru.xxlabaza.service.client.example;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Artem Labazin
 *
 * @since Jul 7, 2015 | 1:02:47 PM
 *
 * @version 1.0.0
 */
@Service
class ClientService {

    @Autowired
    private ClientApi clientApi;

    @Autowired
    private DiscoveryClient discoveryClient;

    @Value("${eureka.instance.metadataMap.instanceId}")
    private String eurekaInstanceId;

    @Value("${spring.application.name}")
    private String applicationName;

    public String askWho () {
        return eurekaInstanceId;
    }

    public String askWhoAnotherService_byFeign () {
        return clientApi.askWho();
    }

    public String askWhoAnotherService_byDiscoveryService () {
        List<ServiceInstance> services = discoveryClient.getInstances(applicationName);
        if (services == null || services.isEmpty()) {
            return "There is no any " + applicationName;
        }

        ServiceInstance firstService = services.get(0);
        String requestUrl = new StringBuilder()
                .append(firstService.getUri().toString())
                .append("/who")
                .toString();

        return new RestTemplate().getForEntity(requestUrl, String.class).getBody();
    }

    @HystrixCommand(fallbackMethod = "fallbackHystrixExample")
    public String hystrixExample (String request) {
        if ("ok".equalsIgnoreCase(request)) {
            return "GOOD EXECUTION HYSTRIX METHOD";
        } else {
            throw new RuntimeException();
        }
    }

    public String fallbackHystrixExample (String request) {
        return "BAD EXECUTION HYSTRIX METHOD with request: " + request;
    }
}
