package ru.xxlabaza.service.client.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 *
 * @author Artem Labazin
 *
 * @since Jul 7, 2015 | 1:02:23 PM
 *
 * @version 1.0.0
 */
@RestController
@RequestMapping(
        method = GET,
        produces = APPLICATION_JSON_VALUE
)
class ClientController {

    @Autowired
    private ClientService clientService;

    @RequestMapping("/who")
    public String askWho () {
        return clientService.askWho();
    }

    @RequestMapping("/feign/who")
    public String askWhoAnotherService_byFeign () {
        return clientService.askWhoAnotherService_byFeign();
    }

    @RequestMapping("/discovery/who")
    public String askWhoAnotherService_byDiscoveryService () {
        return clientService.askWhoAnotherService_byDiscoveryService();
    }

    @RequestMapping("/hystrix/{request}")
    public String hystrixExample (@PathVariable("request") String request) {
        return clientService.hystrixExample(request);
    }
}
