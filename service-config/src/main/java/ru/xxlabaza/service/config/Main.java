package ru.xxlabaza.service.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 *
 * @author Artem Labazin
 *
 * @since Jul 7, 2015 | 11:34:21 AM
 *
 * @version 1.0.0
 */
@EnableConfigServer
@EnableEurekaClient
@SpringBootApplication
public class Main {

    public static void main (String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
