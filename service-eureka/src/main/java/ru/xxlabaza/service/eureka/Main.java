package ru.xxlabaza.service.eureka;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 *
 * @author Artem Labazin
 *
 * @since Jul 7, 2015 | 11:39:13 AM
 *
 * @version 1.0.0
 */
@EnableEurekaClient
@EnableEurekaServer
@SpringBootApplication
public class Main {

    public static void main (String[] args) {
        new SpringApplicationBuilder(Main.class)
                .web(true)
                .run(args);
    }
}
